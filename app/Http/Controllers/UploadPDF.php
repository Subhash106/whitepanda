<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UploadPDF extends Controller
{
    public function uploadPDFAndExtractText(Request $request)
    {
    	
    	$this->validate($request, [
	        'file' => 'required|mimes:pdf|max:200'
	    ]);

    	if ($request->hasFile('file')) 
    	{
    		if(!is_dir(public_path().'/PDFs'))
    		{
    			$path = mkdir(public_path().'/PDFs', 0777, true);
    		}
    		else
    		{
    			$path = public_path().'/PDFs';
    		}
    		$extension = $request->file->extension();
    		$filename = str_random(8).'filename.pdf';
    		$file_path = $path.'/'.$filename;
    		if($extension !== 'pdf')
    		{
    			echo 'Not a pdf file.';
    		}
    		else
    		{
    			$path = $request->file->move($path, $filename);

    			$text = \Spatie\PdfToText\Pdf::getText($file_path);

    			echo '<a href="'.route('welcome').'">Go back to upload new file.</a>';

    			echo '<p>'.$text.'</p>';
    		}

    	}
    }
}
